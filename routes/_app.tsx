import { FreshContext } from "$fresh/server.ts";
import { Partial } from "$fresh/runtime.ts";

export default async function App(req: Request, ctx: FreshContext) {
  return (
    <html>
      <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Simple Material CSS</title>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
        <link rel="stylesheet" href="/simplematerialcss/simplematerial.css" />
        <link rel="stylesheet" href="/styles.css" />
      </head>
      <body f-client-nav>
        <Partial name="body">
          <ctx.Component />
        </Partial>       
      </body>
    </html>
  );
}
