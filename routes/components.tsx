
export default function Components() {
  return (
    <>
      <div className="hero" style="background-image: url('static/components.jpg')">
        <h1>All examples</h1>
        <p>For build your website</p>
        <button>Get started</button>
      </div>

      <h1>Elements</h1>
      <p>Elements that dont need any customization, you can use directly like plain HTML.</p>

      <h2>Titles and text</h2>
      <article className="full">
        <h1>Title 1</h1>
        <h2>Title 2</h2>
        <h3>Title 3</h3>
        <h4>Title 4</h4>
        <p>This is a normal text</p>
        <span>This is a span</span>
      </article>
      <p>Code:</p>
      <pre>
        <code>
          &lt;h1&gt;Title 1&lt;/h1&gt;<br />
          &lt;h2&gt;Title 2&lt;/h2&gt;<br />
          &lt;h3&gt;Title 3&lt;/h3&gt;<br />
          &lt;h4&gt;Title 4&lt;/h4&gt;<br />
          &lt;p&gt;This is a normal text&lt;/p&gt;<br />
          &lt;span&gt;This is a span&lt;/span&gt;<br />
        </code>
      </pre>

      <h2>Buttons</h2>
      <p>The buttons elements are very easy to use, you dont need to add any className or extra properties.</p>

      <article className="full">
        <button>Im a button</button>
      </article>
      <p>Code:</p>
      <pre>
        <code>
          &lt;button&gt;I\'m a button&lt;/button&gt;
        </code>
      </pre>

      <h3>Icon buttons</h3>
      <article className="full">
        <button className="icon">
          <span className="material-symbols-outlined">
            home
          </span>
        </button>
        <button className="icon">
          <span className="material-symbols-outlined">
            star
          </span>
        </button>
        <button className="icon">
          <span className="material-symbols-outlined">
            person
          </span>
        </button>
      </article>
      <p>Code:</p>
      <pre>
        <code>

        </code>
      </pre>

      <h3>Other button styles</h3>
      <article className="full">
        <button className="elevated">I'm a button elevated</button>
        <button className="outline">I'm a button outlined</button><br />
        <button className="primary">I'm a button primary</button>
        <button className="secondary">I'm a button secondary</button>
        <button className="tertiary">I'm a button tertiary</button><br />
        <button className="icon outline">
          <span className="material-symbols-outlined">
            home
          </span>
        </button>
        <button className="icon outline">
          <span className="material-symbols-outlined">
            star
          </span>
        </button>
        <button className="icon outline">
          <span className="material-symbols-outlined">
            person
          </span>
        </button>
      </article>
      <p>Code:</p>
      <pre>
        <code>

        </code>
      </pre>

      <h2>Form elements</h2>
      <p>The form elements require han extra structure have the Material look.</p>
      <article className="full">
        <div className="textfield">
          <label>Label</label>
          <input type="text" placeholder="Normal" />
        </div>

        <div className="textfield outline">
          <label>Label</label>
          <input type="text" placeholder="Outlined" />
        </div>
      </article>

      <p>Code:</p>
      <pre>
        <code>

        </code>
      </pre>

      <h3>Textareas</h3>

      <article className="full">
        <div className="textfield multiline">
          <label>Label</label>
          <textarea rows={3} />
        </div>

        <div className="textfield outline multiline">
          <label>Label</label><br />
          <textarea rows={3}></textarea>
        </div>
      </article>

      <p>Code:</p>
      <pre>
        <code>

        </code>
      </pre>

      <h3>Selects</h3>

      <article className={'full'}>
        <div className="textfield">
          <label>Label</label>
          <select>
            <option value="0">Select car:</option>
            <option value="1">Audi</option>
            <option value="2">BMW</option>
            <option value="3">Citroen</option>
          </select>
        </div>

        <div className="textfield outline">
          <label>Label</label>
          <select>
            <option>Select car:</option>
            <option value="1">Audi</option>
            <option value="2">BMW</option>
            <option value="3">Citroen</option>
          </select>
        </div>
      </article>

      <p>Code:</p>
      <pre>
        <code>

        </code>
      </pre>

      <h3>Checkbox</h3>

      <div>
        <label>Label</label>
        <input type="checkbox" />
      </div>

      <p>Code:</p>
      <pre>
        <code>

        </code>
      </pre>

    </>
  );
}
