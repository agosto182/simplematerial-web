import { PageProps } from "$fresh/server.ts";
import { Footer } from "../components/Footer.tsx";

export default function Layout({ Component, state, route, url }: PageProps) {
  const title = state.title || 'Home';

  function isActive(name: string): string {
    return route === name ? "active" : "";
  }

  return (
    <>
      <header class="sticky">
        <p>{title}</p>
        <div class="right-controls">
          <button class="icon standard">
            <span class="material-symbols-outlined">
              dark_mode
            </span>
          </button>
        </div>
      </header>

      <main>
        <Component />
        <Footer />
      </main>

      <nav class="rail">
        <ul>
          <a href="/">
            <li class={isActive("/")}>
              <i>
                <span class="material-symbols-outlined">
                  home
                </span>
              </i>
              <label>
                Home
              </label>
            </li>
          </a>
          <a href="/components" activeClass="active">
            <li class={isActive("/components")}>
              <i>
                <span class="material-symbols-outlined">
                  design_services
                </span>
              </i>
              <label>
                Components
              </label>
            </li>
          </a>
          <a href="/about">
            <li class={isActive("/about")}>
              <i>
                <span class="material-symbols-outlined">
                  info
                </span>
              </i>
              <label>
                About
              </label>
            </li>
          </a>
        </ul>
      </nav>

      <nav>
        <ul>
          <a href="/">
            <li class={isActive("/")}>
              <i>
                <span class="material-symbols-outlined">
                  home
                </span>
              </i>
              <label>
                Home
              </label>
            </li>
          </a>
          <a href="/components">
            <li class={isActive("/components")}>
              <i>
                <span class="material-symbols-outlined">
                  design_services
                </span>
              </i>
              <label>
                Components
              </label>
            </li>
          </a>
          <a href="about">
            <li class={isActive("/about")}>
              <i>
                <span class="material-symbols-outlined">
                  info
                </span>
              </i>
              <label>
                About
              </label>
            </li>
          </a>
        </ul>
      </nav>
    </>
  );
};
