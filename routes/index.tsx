export default function Home() {
  return (
    <>
      <div class="hero" style="background-image: url('static/home.jpg')">
        <h1>Simple Material</h1>
        <p>The easy way to beatiful your websites</p>
        <button>Get started</button>
      </div>

      <h2>Welcome</h2>
      <p>Simple material is an independient implementation of the <a>Material 3</a>. With the difference that it is very easy to implement.</p>
      <b>No Javascript needed!</b>

      <h2>Nice features</h2>
      <div class="card center outline">
        <div class="card-icon green">
          <span class="material-symbols-outlined">
            raven
          </span>
        </div>
        <h2>Lightwell</h2>
        <p>Only 15kbs</p>
      </div>
      <div class="card center outline">
        <div class="card-icon blue">
          <span class="material-symbols-outlined">
            alarm_on
          </span>
        </div>
        <h2>Easy</h2>
        <p>Very easy to use</p>
      </div>
      <div class="card center outline">
        <div class="card-icon yellow">
          <span class="material-symbols-outlined">
            star
          </span>
        </div>
        <h2>Pretty</h2>
        <p>Do you like Material UI?</p>
      </div>
      <div class="card center outline">
        <div class="card-icon red">
          <span class="material-symbols-outlined">
            square_foot
          </span>  
        </div>
        <h2>Reponsive</h2>
        <p>Mobiles, tablets, and desktop</p>
      </div>

    </>
  );
}
