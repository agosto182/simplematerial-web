
export default function Components() {
  return (
    <>
      <div className="hero" style="background-image: url('static/components.jpg')">
        <h1>All examples</h1>
        <p>For build your website</p>
        <button>Get started</button>
      </div>
    </>
  );
}
